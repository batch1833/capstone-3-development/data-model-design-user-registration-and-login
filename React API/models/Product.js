const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
	name: {
		type: String, 
		required: [true, "Product name is required"]
	},
	description: {
		type: String,
		required: [true, "description is required"]
	},
	price: {
		type: Number,
		required: [true, "price is required"]
	},
	imageURL: {
		type: String,
		default: "" 
	},
	stocks: {
		type: Number,
		required: [true, "stocks is required"]
	},
	isActive: {
		type: Boolean,
		default: true  
	},
	createOn: {
		type: Date,
		default: new Date()
	}
	
});

module.exports = mongoose.model("Product", productSchema);