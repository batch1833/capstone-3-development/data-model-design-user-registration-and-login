//Require modules
const express = require("express");
const mongoose = require("mongoose");

//Cross-origin resource sharing (CORS)
const cors = require("cors");

// need to require userRoutes before creating routes for our api
const userRoutes = require("./routes/userRoutes");
const productRoutes =require("./routes/productRoutes");

//Create Server
const app = express();
const port = 4000;

//Connect to our MongoDB Database
mongoose.connect("mongodb+srv://admin:admin@cluster0.mmogcml.mongodb.net/react-ecommerce-app?retryWrites=true&w=majority", {
		useNewUrlParser: true,
		useUnifiedTopology: true
});


//Set notification for success or failure
let db = mongoose.connection;
db.on("error", console.error.bind(console, "Connection error"));

db.once("open", () => console.log("We're connected to the cloud database."));

//Middlewares
// to allow all resources to access our backend application from any endpont either local or hosted application.
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));

//Routes for our API
//localhost:4000/users
app.use("/users", userRoutes);
app.use("/products", productRoutes);

//Listening to port
app.listen(process.env.PORT || port, () => {
	console.log(`API is now online on port ${process.env.PORT || port}`);
})

