const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");

const Product = require("../models/Product");
const Order = require("../models/Order");

// Check if the email already exists
/*
	Steps:
	1. Use mongoose "find" method to find the duplicate emails.
	2. Use the ".then" method to send back a response to the front end application based on the result of find method.

*/

module.exports.checkEmailExists = (reqBody) => {
	return User.find({email: reqBody.email}).then(result => {
		// result is equal to an empty array ([]);
		// if result is greater than 0, user is found/already exists.
		if(result.length > 0){
			return true;
		}
		// No duplicate email found
		else{
			return false;
		}
	});
}

// Router for the user registration
module.exports.registerUser= (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result => {
		if(result != null && result.email == reqBody.email){
			//return "Already registered!";
			return false;
		}
		else{
			if(reqBody.email != "" && reqBody.password != ""){
				let newUser = new User({
					firstName: reqBody.firstName,
					lastName: reqBody.lastName,
					email: reqBody.email,
					password: bcrypt.hashSync(reqBody.password, 10), 
					isAdmin: reqBody.isAdmin
				})
				return newUser.save().then((user, error) =>{
						if(error){
							//return "Registration failed!";
							return false;
						}
						else{
							console.log(newUser);
							//console.log(`${newUser.email} is successfully registered.`);
							//const id = user._id;
							//return "Successfully registered! Your user id is: " + "" + id.toString();
							return true;
						}
					})
			}
			else{
				//return "Missing field, cannot proceed Registration !"; 
				return false;
			}
		}
	})
}


//Retrieve all users
module.exports.retrieveAllUsers = () =>{
	return User.find({}).then(result => result);
}


//Route for the user login(with token creation)
//User authentication
module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result =>{

		//User does not exist
		if(result == null){
			//return "User does not exsist";
			return false;
		}
		
		else{
	
			// Syntax: bcrypt.compareSync(data, encrypted);
			// reqBody.password & result.password
			// bcrypt.compareSync() return Boolean
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			
			if(isPasswordCorrect){
				//generating a token from auth.js
				const access = auth.createAccessToken(result); 
				//return "WELCOME BACK! " + " " + " Your access token is below" + " " + " : " + access.toString();
				return {access: auth.createAccessToken(result)}
			}
			
			else{
				return "password does not match";
				//return false;
			}
		}
	})
}

// Retrieve user details
/*
	Steps:
	1. Find the document in the database using the user's ID.
	2. Reassign the password of the result document to an empty string("").
	3. Return the result back to the frontend.
*/
module.exports.getProfile = (data) =>{
	console.log(data)
	return User.findById(data.userId).then(result =>{
		result.password ="";

		return result;
	})
}

//set a user to admin(admin only)
module.exports.setAdmin = (userId) =>{

	let updatedtoAdmin = {
		isAdmin: true
	}

	return User.findByIdAndUpdate(userId, updatedtoAdmin).then((updatedtoAdmin, error) =>{
		if (error) {
			return false;
		}
		else {
			//true
			//const firstName = reqBody.firstName;
			const id = userId;
			//return "Successfully added as admin user: " + "" + id.toString();
			return true;
		}
	})
	
}

//Create Order - Customer only
module.exports.createOrder = async (data) =>{
	console.log(data);
let newOrder 
	let isOrderUpdated = await Order.findById(data.userId).then(orderResult =>{
				
				newOrder = new Order({
					userId: data.userId,
					productId: data.productId,
					price: data.price,
					quantity: data.quantity,
					total: data.total
				})
				return newOrder.save().then((order, error) =>{
					if(error){
						return false;
						}
					else{
						return true;
					}	
				})
		})
	let isProductUpdated = await Product.findById(data.productId).then(product =>{
		product.stocks = product.stocks - data.quantity;
		return product.save().then((stocks, error) =>{
			if(error){
				return false;
			}
			else{
				return true;
			}
		})
	})

	let isPriceUpdated = await Product.findById(newOrder.productId).then(priceResult =>{
		newOrder.price = priceResult.price;
		newOrder.total = priceResult.price * newOrder.quantity;
		return newOrder.save().then((order, error)=>{
			if(error){
				return false;
			}
			else{
				return true;
			}
		})
	})
	console.log(newOrder);
	if(isOrderUpdated && isProductUpdated && isProductUpdated){
		
		return "Your Order has been placed"
		//return true;
	}
	else{
		return "Unsuccessful Order"
		//return false;
	}

}


// Retrieve User’s orders (User) 
module.exports.getUserOrders = (data) =>{
	return Order.find({userId: data.userId}).then(result => result);
}

	
// Retrieve all orders (Admin only)
module.exports.getAllOrders = () =>{
	return Order.find({}).then(result => result);
}


