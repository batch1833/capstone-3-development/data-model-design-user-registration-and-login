import {useState, useEffect} from "react";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import { UserProvider } from "./UserContext";

import AppNavbar from "./components/AppNavbar";
import AdminDashboard from "./pages/AdminDashboard";
import Products from "./pages/Products";
import ProductView from "./pages/ProductView";
import Error from "./pages/Error";
import Home from "./pages/Home";
import Login from "./pages/Login";
import AddOrder from "./pages/AddOrder";
import Logout from "./pages/Logout";
import Register from "./pages/Register";
import AddProduct from "./pages/AddProduct";
import EditProduct from "./pages/EditProduct";
import {Container} from "react-bootstrap";
import './App.css';
import Cart from "./pages/Cart";
import { CartProvider } from "react-use-cart";
import ShowOrder from "./pages/ShowOrder";
import CartView from "./components/CartView";
function App() {

   //Global state - means accessable sa lahat 
  //To store the user information and will be used for validating if a user is already logged in on the app or not.
  const [user, setUser] = useState({
            //null
    //email: localStorage.getItem("email")
    id: null,
    isAdmin: null
  })

  console.log(user);

  //Function for clearing localStorage on logout
  const unsetUser = () =>{
    localStorage.clear();
  }

  useEffect(() =>{
    console.log(user);
    console.log(localStorage);
  }, [user])

  //To update the User State upon page load if a user already exist.
  useEffect(() =>{
    fetch(`${process.env.REACT_APP_API_URL}/users/details`,{
      headers:{
        Authorization: `Bearer ${localStorage.getItem("token")}`
      }
    })
    .then(res => res.json())
    .then(data => {
      console.log(data);

      //Set the user states values with the user deails upon successful login
      if(typeof data._id !== "undefined"){
          setUser({
            //undefined
            id: data._id,
            isAdmin: data.isAdmin
          })
      }
      else{
          setUser({
              //undefined
              id: null,
              isAdmin: null
            })
      }
    })
  
  }, [])

  return (

    
    <UserProvider value={{user, setUser, unsetUser}}>
      {/*  // Router component is used to wrapped around all components which will have access to our routing system.*/}
      <Router>
      <AppNavbar />
      
          {/*Routes holds all our Route components.*/}
          <Routes>
          {/*
              - Route assigns an endpoint and displays the appropriate page component for that endpoint.
              - "path" attribute assigns the endpoint.
              - "element" attribute assigns page component to be displayed at the endpoint.
          */}
              <Route exact path ="/" element={<Home />} />
              <Route exact path ="/admin" element={<AdminDashboard />} />
              <Route exact path ="/addProduct" element={<AddProduct />} />
              <Route exact path ="/addorder" element={<AddOrder />} />
              <Route exact path ="/cartview" element={<CartView />} />
              <Route exact path ="/editProduct/:productId" element={<EditProduct />} />
              <Route exact path ="/products" element={<Products />} />
              <Route exact path ="/products/:productId" element={<ProductView />} />
              <Route exact path ="/register" element={<Register />} />
              <Route exact path ="/showorder" element={<ShowOrder />} />
              <Route exact path ="/login" element={<Login />} />
              <Route exact path ="/logout" element={<Logout />} />
              <Route exact path ="*" element={<Error />} />
          </Routes>
     
      </Router>
    </UserProvider>

  );
}



export default App;

