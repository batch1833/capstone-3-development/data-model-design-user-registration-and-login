import {Row, Col, Button} from "react-bootstrap";
import {Link} from "react-router-dom";
import Carousel from 'react-bootstrap/Carousel';

export default function Banner({data}){
	console.log(data);

	// Destructuring refer to errorjs
	const {title, content, destination, label} = data;

	return(
		<Row className="background1">
			<Col className="p-5 text-center">

				<h1 className="bannertitle">{title}</h1>
				<p>{content}</p>
				<Button as={Link} to={destination} variant="secondary">{label}</Button>
			</Col>
			<Carousel variant="dark" >
				  <Carousel.Item>

						<div class="row">
						            <div class="col-lg-6 mb-4">				  
										    <img

										      className="d-block w-55"
										      src="./images/bik1.png" class="img-fluid" alt="Responsive image"
										    />

					    			</div>
							<div class="col-lg-6 mt-5">
									<p className="slidetitle">Get the</p>
								    <h1 className="slidetitle2" >latest updates	
								    </h1 >
								    <h3 className="slidetitle3">promo and discounts.</h3>
							</div>
						</div>
   			</Carousel.Item>
			  <Carousel.Item>
			  <div class="row">
			  			            <div class="col-lg-6 mb-4">				  
			  							    <img

			  							      className="d-block w-55"
			  							      src="./images/bik2.png" class="img-fluid" alt="Responsive image"
			  							    />
			  						</div>
			  				<div class="col-lg-6 mt-5">
			  						<p className="slidetit">You create</p>
			  					    <h1 className="slidetit2">the journey	
			  					    </h1>
			  					    <h3 className="slidetit3">we supply the parts.</h3>
			  				</div>
			  			</div>  </Carousel.Item>

			  <Carousel.Item>
			  <div class="row">
			  			            <div class="col-lg-6 mt-auto ">				  
			  							    <img

			  							      className="d-block w-55"
			  							      src="./images/bik3.png" class="img-fluid" alt="Responsive image"
			  							      
			  							    />
			  				
			  		    </div>
			  				<div class=" col-lg-6 mt-5">
			  						<p className="slidetit">To keep </p>
			  					    <h1 className="slidetit2">your Balance</h1>
			  					    <h3 className="slidetit3">must keep moving.</h3>


			  				</div>
			  			</div>
			  </Carousel.Item>
			</Carousel>

		</Row>
	)
}