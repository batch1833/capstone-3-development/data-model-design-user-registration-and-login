import { useContext, useState, useEffect } from "react";
import { Modal, Button, Tooltip, Form } from "react-bootstrap";
import { Link } from "react-router-dom";
import Swal from "sweetalert2"

export default function ProductData({product}) {
	console.log(product)

	const [productId, setProductId] = useState(product._id);
	const [name, setName] = useState(product.name);
    const [description, setDescription] = useState(product.description);
    const [price, setPrice] = useState(product.price);
    const [stocks, setStocks] = useState(product.stocks);
    const [isActive, setIsActive] = useState(product.isActive);


    const [show, setShow] = useState(false);
    const handleShow = () => setShow(true);
    

	// Product archive function 
	const archive = (productId, productName) => {
		fetch(`http://localhost:4000/products/${productId}/archive`, {
			method: "PATCH",
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				isActive: false
			})
		})
		.then(res => res.json())
		.then(data => {
			if (data) {

				Swal.fire({
					title: "Archive Successful!",
					icon: "success",
					text: `${productName} is now inactive`
				})
				setIsActive(false)

			}
			else {
				Swal.fire({
					title: "Something went wrong!",
					icon: "error",
					text: `Please try again`
				})

			}
		})
	}

	// Product unarchive function
	const unarchive = (productId, productName) => {
		console.log(productId)
		console.log(productName)

		fetch(`http://localhost:4000/products/${productId}/archive`, {
			method: "PATCH",
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				isActive: true
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			if (data) {
				Swal.fire({
					title: "Unarchive Successful!",
					icon: "success",
					text: `${productName} is now active`
				})
				setIsActive(data);
			}
			else {
				Swal.fire({
					title: "Something went wrong!",
					icon: "error",
					text: `Please try again`
				})

			}
		})
	}


	// Function for Close Modal Edit
	const closeEdit = () => {
		fetch(`${process.env.REACT_APP_API_URL}/products/${ productId }`)
		.then(res => res.json())
		.then(data => {
			console.log(data)

			//populate all input values with the course information that we fetched
			setProductId(data._id)
			setName(data.name)
			setDescription(data.description)
			setPrice(data.price)
		})

		setShow(false)
	}


	//a function to change or update the specific course
	const editProduct = (e, productId) => {
		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/products/${ productId }`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${ localStorage.getItem('token') }`
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data === true){
				Swal.fire({
					title: 'Success',
					icon: 'success',
					text: 'Course successfully updated'
				})
				// fetchData()
				closeEdit()
			}else {
				Swal.fire({
					title: 'error',
					icon: 'error',
					text: 'Please try again'
				})

				// fetchData()
				closeEdit()
			}
		})
	}

    return (
    	 <>
    	        <td>{product._id}</td>
    	        <td>{name}</td>
    	        <td>{description}</td>
    	        <td>{price}</td>
    	        <td>{stocks}</td>
    	        <td>{isActive ? "Available" : "Unavailable"}</td>
    	        <td>
					{
						
						(isActive)
						?
						<>
							<Button variant="danger" size="sm" onClick={() => archive(product._id,name)}>Archive</Button>
						</>
						:
						<>
							<Button variant="success" size="sm" onClick={() => unarchive(product._id,product.name)}>Unarchive</Button>{' '}
							{/*<Button variant="secondary" size="sm"  onClick={handleShow}>Edit</Button>*/}
							<Button variant="primary" size="sm" onClick={handleShow}>Update</Button>
						</>
						
					}
				</td>

    	        <Modal show={show} onHide={closeEdit} backdrop="static" keyboard={false}>
    	        	<Form onSubmit={e => editProduct(e, product._id)}>
    	        		<Modal.Header closeButton>
    	        			<Modal.Title>Edit Product</Modal.Title>
    	        		</Modal.Header>

    	        		<Modal.Body>
    	        			<Form.Group>
    	        				<Form.Label>Name</Form.Label>
    	        				<Form.Control 
    	        				      type="text"
    	        				      required
    	        				      value={name}
    	        				      onChange={e => setName(e.target.value)}
    	        				 />
    	        			</Form.Group>

    	        			<Form.Group>
    	        				<Form.Label>Description</Form.Label>
    	        				<Form.Control 
    	        				      type="text"
    	        				      required
    	        				      value={description}
    	        				      onChange={e => setDescription(e.target.value)}
    	        				 />
    	        			</Form.Group>

    	        			<Form.Group>
    	        				<Form.Label>Price</Form.Label>
    	        				<Form.Control 
    	        				      type="number"
    	        				      required
    	        				      value={price}
    	        				      onChange={e => setPrice(e.target.value)}
    	        				 />
    	        			</Form.Group>
    	        		</Modal.Body>

    	        		<Modal.Footer>
    	        			<Button variant="secondary" onClick={closeEdit}>Close</Button>
    	        			<Button variant="success" type="submit">Submit</Button>
    	        		</Modal.Footer>

    	        	</Form>
    	        </Modal>
    	    </>
    )

}
