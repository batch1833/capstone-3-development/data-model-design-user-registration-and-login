import { Link } from "react-router-dom";

import {useState, useContext} from "react";
import UserContext from "../UserContext"

import {Navbar, Nav, Container, NavDropdown} from "react-bootstrap";

export default function AppNavbar(){

  const {user} = useContext(UserContext);

  
  return(
         <div className="navbarTop">
            
            <nav>
               <Navbar.Brand as={Link} to="/" >
                  <img src="./images/logo.png" width="50" height="40" alt="logo"/>Home</Navbar.Brand>
                    
                  <ul>      
                    {
                        (user.isAdmin)
                        ?
                        <li><Nav.Link as={Link} to="/admin" eventKey="/admin">Admin Dashboard</Nav.Link></li>
                        :
                        <li><Nav.Link as={Link} to="/products" eventKey="/products">All Products</Nav.Link></li>
                    }

                    { 
                      (user.id !== null)
                      ?
                        <li><Nav.Link as={Link} to="/logout" eventKey="/logout">Logout</Nav.Link></li>
                      :
                        <>
                          <li><Nav.Link as={Link} to="/login" eventKey="/login">Login</Nav.Link></li>
                          <li><Nav.Link as={Link} to="/register" eventKey="/register">Register</Nav.Link></li>
                        </>
                    }
                    
                   {
                    (user.id !== null &&  user.isAdmin !== true)
                    ?
                      <li><Nav.Link as={Link} to="/cartView" eventKey="/cartView"><img src="./images/cart.png" alt="cart.icon"/></Nav.Link></li>
                    :
                      <img src="./images/cart.png" alt="cart.icon"/> == null
                   }
                
                  </ul>
               
             </nav>
          </div>

    )


}
