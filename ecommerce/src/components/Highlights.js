import {Row, Col } from "react-bootstrap";
import Card from 'react-bootstrap/Card';
import CardGroup from 'react-bootstrap/CardGroup';


export default function Highlights() {
	return(
		<Row className="otherServices mt-5 mb-5 " >
		  
    <h1 className="text-center mt-5">Other Services</h1>
    <CardGroup className="mt-5 mb-3  ">

      <Card border="success" className="cardHighlight p-3">
        <Card.Img variant="top" src="./images/onSale.jpg" width="200" height="350" />
        <Card.Body>
          <Card.Title>Card title</Card.Title>
          <Card.Text>
            This is a wider card with supporting text below as a natural lead-in
            to additional content. This content is a little bit longer.
          </Card.Text>
        </Card.Body>
        <Card.Footer>
          <small className="text-muted">Last updated 3 mins ago</small>
        </Card.Footer>
      </Card>

      <Card border="success" className="cardHighlight p-3">
        <Card.Img variant="top" src="./images/bicycle marathon.jpg" width="200" height="350"/>
        <Card.Body>
          <Card.Title>Card title</Card.Title>
          <Card.Text>
            This card has supporting text below as a natural lead-in to
            additional content.{' '}
          </Card.Text>
        </Card.Body>
        <Card.Footer>
          <small className="text-muted">Last updated 3 mins ago</small>
        </Card.Footer>
      </Card>

      <Card border="success" className="cardHighlight p-3">
        <Card.Img variant="top" src="./images/parts.jpg"width="200" height="350" />
        <Card.Body>
          <Card.Title>Card title</Card.Title>
          <Card.Text>
            This is a wider card with supporting text below as a natural lead-in
            to additional content. This card has even longer content than the
            first to show that equal height action.
          </Card.Text>
        </Card.Body>
        <Card.Footer>
          <small className="text-muted">Last updated 3 mins ago</small>
        </Card.Footer>
      </Card>
    </CardGroup>

</Row>

	)
}



