import { useState, useEffect, useContext } from "react";
import React from "react";
import { useCart } from "react-use-cart";
import {Navigate, useNavigate} from "react-router-dom";
import Swal from 'sweetalert2';


const Cart = () =>{

	const navigate = useNavigate();

	const { isEmpty, totalUniqueItems, items, totalItems, cartTotal, updateItemQuantity, removeItem, emptyCart } = useCart();
	if(isEmpty) return <h1 className="text-center">Your Cart is Empty!</h1>

	const list = JSON.parse(localStorage.getItem('react-use-cart'));
    console.log(list.items)

	function checkout(productProp){

			const cartitem = list.items.map(item => {
			//console.log(list)
				
			fetch(`${process.env.REACT_APP_API_URL}/users/addOrder`,{
	            method: "POST",
	            headers:{
	                "Content-Type" : "application/json",
	                Authorization: `Bearer ${localStorage.getItem("token")}`
	            },
	            body: JSON.stringify({
					productId: item.id,
					productName: item.name,
					price: item.price,
					quantity: item.quantity
	            })
	        })
	        .then(res => res.json())
	        .then(data => {
	           console.log(data);

	            if(data === true){
	                Swal.fire({
	                    title: "Item successfully added",
	                    icon: "success",
	                    text: "This item has been added to your cart"
	                })
	                navigate("/ShowOrder");
	            }else{
	                Swal.fire({
	                    title: "Login to order",
	                    icon: "error",
	                    text: "Please try again."
	                })
	            }
	        })
		})
	}

	return(
		<section className="py-4 container">
			<div className="row justify-content-center">
				<div className="col-12">
					<h5 className="fw-bold">Cart ({totalUniqueItems}) Total Items: ({totalItems})</h5>
					<table className="table table-light table-hover m-0">
					<tbody>
						{items.map((item, index)=>{
					return(
						<tr key={index}>
							<td>{item.name}</td>
							<td>{item.price}</td>
							<td>Quantity ({item.quantity})</td>
							<button className="btn btn-primary text-danger ms-2 mt-2" onClick={()=> updateItemQuantity(item.id, item.quantity - 1)}>-</button>
							<button className="btn btn-primary text-warning ms-2 mt-2" onClick={()=> updateItemQuantity(item.id, item.quantity + 1)}>+</button>
							<button size="sm" className="btn btn-danger text-primary ms-2 fw-bold" onClick={()=>removeItem(item.id)}>Remove Item</button>
						</tr>
							)
						})
						}

					</tbody>
					</table>
				</div>
				<div className="mt-4"></div>
				<hr/>
				<div className="col-auto ms-auto ">
					<h2 className="fw-bold mt-3">Total Price: &#8369;{cartTotal}</h2>
				</div>
				<div className="col-auto mt-2">
					{/*<button className="btn btn-danger m-2 fw-bold" size="sm"onClick={()=> emptyCart()}>Clear Cart</button>
					
					<button className="btn btn-primary m-2 fw-bold" size="sm" onClick={() => checkout()}>Check Out</button>*/}
					<button className="btn btn-danger m-2 fw-bold" size="sm" onClick={()=> emptyCart()}>Clear Cart</button>
					<button className="btn btn-primary m-2 fw-bold" size="sm" onClick={() => checkout()}>Check Out</button>
				</div>
			</div>
		</section>
		)
}
export default Cart;
