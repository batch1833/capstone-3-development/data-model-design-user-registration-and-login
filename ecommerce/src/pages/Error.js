import Highlights from "../components/Highlights";

export default function Error(){
	const data = {
		title: "404 - Page not Found",
		content: "The page you are looking for cannot be found.",
		destination: "/",
		label: "Back home"
	}
	return(
		<Highlights data={data} />
	)
}