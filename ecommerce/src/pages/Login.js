import {useState, useEffect, useContext} from "react";
import UserContext from "../UserContext";

import { Navigate } from "react-router-dom";
import { Form, Button, Container} from "react-bootstrap";
import Swal from "sweetalert2";

export default function Login(){

	//Allows us to consume the User Context object and it's properties to use for the user validation.
	const {user, setUser} = useContext(UserContext);

	//State hooks to sotr the values of the input fields
	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");

	//State to determine whether the button is enabled or not
	const [isActive, setIsActive] = useState(false);

	function login(e){
		//prevents the page redirection via form submit
		e.preventDefault();

			fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
				method: "POST",
				headers: {
					"Content-Type": "application/json"
				},
				body: JSON.stringify({
					// values are coming from our state hooks
					email: email,
					password: password
				})
			})
			.then(res => res.json())
			.then(data =>{
				console.log(data);
				console.log(typeof data.access)

			if(typeof data.access !== "undefined"){
				localStorage.setItem("token", data.access);
				retrieveUserDetails(data.access);

				Swal.fire({
					title: "Login Succesful",
					icon: "success",
					text: `${email} has been verified! Welcome back!`
				});
			}
			else{
				Swal.fire({
					title: "Authentication Failed",
					icon: "error",
					text: "Check your login details and try again."
			});
		}
	})


		//Clear input fields
		setEmail("");
		setPassword("");

		// alert(`${email} has been verified! Welcome back!`);

	}

	//Retrieve user details
	// we will get the payload from the token. 
	const retrieveUserDetails = (token) => {
		fetch(`${process.env.REACT_APP_API_URL}/users/details`,{
			headers:{
				Authorization: `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			//This will be set to the user state.
			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			})
		})
	}


	useEffect(() =>{
		// Validation to enable the submit button when all fields are populated.
		if(email !== "" && password !== ""){
			setIsActive(true)
		}
		else{
			setIsActive(false)
		}
	}, [email, password])

	return(
		
		(user.id !== null)
		?
			
			<Navigate to="/products"/>
		:
		<>
		<Container className="logbg">
			<h1 className="my-5 text-center">Login</h1>
			<div className="d-flex justify-content-center align-items-center">
			<Form className="rounded p-4 p-sm-3" onSubmit ={(e) => login(e)}>
				<Form.Group className="mb-3" controlId="userEmail">
				  <Form.Label>Email address</Form.Label>
				  <Form.Control type="email" placeholder="Enter email" value={email} onChange={e => setEmail(e.target.value)}/>
				  <Form.Text className="text-muted">
				    We'll never share your email with anyone else.
				  </Form.Text>
				</Form.Group>

				<Form.Group className="mb-3" controlId="password">
				  <Form.Label>Password</Form.Label>
				  <Form.Control type="password" placeholder="Password" value={password} onChange={e => setPassword(e.target.value)}/>

				</Form.Group>
					
				{
					isActive
					?
						<div className="d-grid gap-2">
						<Button variant="danger" type="submit" id="submitBtn">
						  Submit
						</Button>
						</div>
					:
						<div className="d-grid gap-2">
						<Button variant="primary" type="submit" id="submitBtn" disabled>
						  Submit
						</Button>
						</div>
				}
				<a href="http://localhost:3000/register">Register again</a>
		
			</Form>
			</div>
			
		</Container>
		</>
	)
}