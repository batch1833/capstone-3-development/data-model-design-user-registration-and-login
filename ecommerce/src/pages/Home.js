import Banner from "../components/Banner";

import Highlights from "../components/Highlights"


export default function Home() {
	const data = {
		title: "My Ecommerce Website ",
		/*content: "Bikes for everyone, everywhere",*/
		destination: "/products",
		label: "Our products"
	}
	return(
		<>
			<Banner data={data}/>
			<Highlights />

		</>
	)
}