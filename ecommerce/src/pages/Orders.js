
import Card from 'react-bootstrap/Card';
import Cart from "../pages/Cart";


return(

			<div className="mt-4"></div>
				<hr/>
				<div className="col-auto ms-auto ">
					<h2 className="fw-bold mt-3">Total Price: &#8369;{cartTotal}</h2>
				</div>
				<div className="col-auto mt-2">
					<button className="btn btn-danger m-2 fw-bold" size="sm" onClick={()=> emptyCart()}>Clear Cart</button>
					<button className="btn btn-primary m-2 fw-bold" size="sm" onClick={()=> checkout()}>Buy Now</button>
				</div>
		
)
export default Orders;